#include "cyber/cyber.h"
#include "cyber/zhum/proto/messages.pb.h"

using apollo::cyber::zhum::proto::Driver;

int main(int argc, char* argv[])
{
    apollo::cyber::Init(argv[0]);
    std::shared_ptr<apollo::cyber::Node> node(
        apollo::cyber::CreateNode("service_node"));
    auto server = node->CreateService<Driver, Driver>(
        "test_server", [](const std::shared_ptr<Driver>& request, 
                          std::shared_ptr<Driver>& response) {
            AINFO << "server: I am driver server";
            static uint64_t id = 0;
            ++id;
            response->set_msg_id(id);
            response->set_timestamp(0);
        });
    auto client = node->CreateClient<Driver, Driver>("test_server");
    auto driver_msg = std::make_shared<Driver>();
    driver_msg->set_msg_id(0);
    driver_msg->set_timestamp(0);
    apollo::cyber::Rate rate(1.0);
    while (apollo::cyber::OK())
    {
        auto res = client->SendRequest(driver_msg);
        if (res != nullptr)
        {
            AINFO << "client: response: " << res->ShortDebugString();
        } 
        else 
        {
            AINFO << "client: service may not ready.";    
        }
        rate.Sleep();
    }

    apollo::cyber::WaitForShutdown();
    return 0;
}
